public class PokolorowanyOsobnik extends Osobnik {
	private int liczbaKolorow;
	private int liczbaKonfliktow;
	private int[] koloryWierzcholow;

	public PokolorowanyOsobnik(Osobnik o, int liczbaKolorow) {
		super(o.getTablicaKrawedzi());
		this.liczbaKolorow = liczbaKolorow;
		koloryWierzcholow = new int[o.getLiczbaWierzcholkow()];
	
		for (int i = 0; i < koloryWierzcholow.length; i++)
			koloryWierzcholow[i] = generator.nextInt(liczbaKolorow);
		
		for (int i = 0; i < o.getTablicaKrawedzi()[0].length ; i++)
			if (koloryWierzcholow[o.getTablicaKrawedzi()[0][i]-1] == koloryWierzcholow[o
					.getTablicaKrawedzi()[1][i]-1]){
				liczbaKonfliktow++;		
			}
		//zakomentowa� dla osobnik�w o tablicy maj�cej po�owe
		liczbaKonfliktow /=2;
	}
	
	public PokolorowanyOsobnik(Osobnik o, int[] koloryWierzcholkow, int liczbaKolorow) {
		super(o.getTablicaKrawedzi());
		
		this.liczbaKolorow = liczbaKolorow;
		
		this.koloryWierzcholow = koloryWierzcholkow;
	
		
		for (int i = 0; i < o.getTablicaKrawedzi()[0].length ; i++)
			if (koloryWierzcholow[o.getTablicaKrawedzi()[0][i]-1] == koloryWierzcholow[o
					.getTablicaKrawedzi()[1][i]-1]){
				liczbaKonfliktow++;		
			}
		//zakomentowa� dla osobnik�w o tablicy maj�cej po�owe
		liczbaKonfliktow /=2;
	}
	
	public PokolorowanyOsobnik(Osobnik o, int[] koloryWierzcholkow) {
		super(o.getTablicaKrawedzi());
	
		
		this.koloryWierzcholow = koloryWierzcholkow;
	
		
		for (int i = 0; i < o.getTablicaKrawedzi()[0].length ; i++)
			if (koloryWierzcholow[o.getTablicaKrawedzi()[0][i]-1] == koloryWierzcholow[o
					.getTablicaKrawedzi()[1][i]-1]){
				liczbaKonfliktow++;		
			}
		//zakomentowa� dla osobnik�w o tablicy maj�cej po�owe
		liczbaKonfliktow /=2;
	}
	
	
	

	public int getLiczbaKolorow() {
		return liczbaKolorow;
	}

	public void setLiczbaKolorow(int liczbaKolorow) {
		this.liczbaKolorow = liczbaKolorow;
	}

	public int[] getKoloryWierzcholow() {
		return koloryWierzcholow;
	}

	public void setKoloryWierzcholow(int[] koloryWierzcholow) {
		this.koloryWierzcholow = koloryWierzcholow;
	}

	public int getLiczbaKonfliktow() {
		return liczbaKonfliktow;
	}

	public void setLiczbaKonfliktow(int liczbaKonfliktow) {
		this.liczbaKonfliktow = liczbaKonfliktow;
	}

	@Override
	public void wyswietlKrawedzie() {
		for (int i = 0; i < this.getTablicaKrawedzi()[0].length; i++)
			System.out.println("Kraw�dzie: " + this.getTablicaKrawedzi()[0][i]
					+ " " + this.getTablicaKrawedzi()[1][i] + " Kolory:  "
					+ koloryWierzcholow[this.getTablicaKrawedzi()[0][i] -1]
					+ " "
					+ koloryWierzcholow[this.getTablicaKrawedzi()[1][i] - 1]); // -1
																				// bo
																				// tam
																				// mam
																				// od
																				// 1
	}

}
