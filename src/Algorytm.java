import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

public class Algorytm {
	static Random generator = new Random();
	private static String nazwaGrafu;
	private static int licznoscPopulacji;
	private static int iloscKrzyzowan;

	private static PrintWriter max = null;
	private static PrintWriter min = null;
	private static PrintWriter avg = null;

	public static int[][] wczytajGraf(String nazwa) throws IOException {
		int lKrawedzi;
		int lWierzcholkow;
		BufferedReader bufferedReader = new BufferedReader(new FileReader(
				"C:\\" + nazwa + ".txt"));
		int[][] graf;

		try {
			String textNaglowek = bufferedReader.readLine();
			String[] naglowek = textNaglowek.split(" ");
			lKrawedzi = Integer.parseInt(naglowek[1].trim());
			lWierzcholkow = Integer.parseInt(naglowek[0].trim());
			graf = new int[2][lKrawedzi];

			String textLine = bufferedReader.readLine();
			int j = 0;
			do {

				String[] parts = textLine.split(" ");

				graf[0][j] = Integer.parseInt(parts[1].trim());
				graf[1][j] = Integer.parseInt(parts[2].trim());
				j++;

				textLine = bufferedReader.readLine();
			} while (textLine != null);
		} finally {
			bufferedReader.close();
		}

		return graf;
	}

	public static void wezDane(String nazw, int popul, int krzyz) {
		nazwaGrafu = nazw;
		licznoscPopulacji = popul;
		iloscKrzyzowan = krzyz;

	}

	public static int dajLKrawedzi(String nazwa) throws IOException {
		int lKrawedzi;
		BufferedReader bufferedReader = new BufferedReader(new FileReader(
				"C:\\" + nazwa + ".txt"));

		String textNaglowek = bufferedReader.readLine();
		String[] naglowek = textNaglowek.split(" ");
		lKrawedzi = Integer.parseInt(naglowek[1].trim());
		return lKrawedzi;
	}

	public static int dajLWierzcholkow(String nazwa) throws IOException {
		int lWierzcholkow;
		BufferedReader bufferedReader = new BufferedReader(new FileReader(
				"C:\\" + nazwa + ".txt"));

		String textNaglowek = bufferedReader.readLine();
		String[] naglowek = textNaglowek.split(" ");
		lWierzcholkow = Integer.parseInt(naglowek[0].trim());
		return lWierzcholkow;
	}

	public static PokolorowanyOsobnik krzyzuj(PokolorowanyOsobnik os1,
			PokolorowanyOsobnik os2) {
		int dlugosc = os1.getKoloryWierzcholow().length;
		int polowaDlugosci = dlugosc / 2;
		int[] tablicaKol = new int[dlugosc];

		System.arraycopy(os1.getKoloryWierzcholow(), 0, tablicaKol, 0,
				polowaDlugosci);
		System.arraycopy(os2.getKoloryWierzcholow(), polowaDlugosci,
				tablicaKol, polowaDlugosci, polowaDlugosci);
		// System.arraycopy(os2.getKoloryWierzcholow(),
		// polowaDlugosci,tablicaKol, polowaDlugosci, polowaDlugosci +1 );
		/*
		 * System.out.println("-------------------------------"); for (int i =
		 * 0; i < dlugosc; i++) System.out.println(tablicaKol[i]);
		 * System.out.println("-------------------------------");
		 */
		return new PokolorowanyOsobnik(os1, tablicaKol, os1.getLiczbaKolorow());

	}

	public static PokolorowanyOsobnik mutuj(PokolorowanyOsobnik os) {
		int[] tablicaKol = os.getKoloryWierzcholow();
		tablicaKol[generator.nextInt(os.getKoloryWierzcholow().length)] = generator
				.nextInt(os.getLiczbaKolorow());
		os.setKoloryWierzcholow(tablicaKol);

		int liczbaKonf = 0;
		for (int i = 0; i < os.getTablicaKrawedzi()[0].length; i++)
			if (tablicaKol[os.getTablicaKrawedzi()[0][i] - 1] == tablicaKol[os
					.getTablicaKrawedzi()[1][i] - 1]) {
				liczbaKonf++;
				os.setLiczbaKonfliktow(liczbaKonf);

			}
		return os;
	}

	/* public static int ilicznik = 0; */
	public static PokolorowanyOsobnik turniej(PokolorowanyOsobnik[] osobnicy,
			int ilosc) {
		PokolorowanyOsobnik[] turniej = new PokolorowanyOsobnik[ilosc];
		PokolorowanyOsobnik[] zwyciezcy = new PokolorowanyOsobnik[2];
		for (int i = 0; i < turniej.length; i++)
			turniej[i] = osobnicy[generator.nextInt(osobnicy.length)];
		zwyciezcy[0] = turniej[0];
		zwyciezcy[1] = turniej[1];

		for (int i = 0; i < turniej.length; i++) {

			if (zwyciezcy[0].getLiczbaKonfliktow() > turniej[i]
					.getLiczbaKonfliktow() * generator.nextDouble() + 5)
				zwyciezcy[0] = turniej[i];
			else if (zwyciezcy[1].getLiczbaKonfliktow() > turniej[i]
					.getLiczbaKonfliktow())
				zwyciezcy[1] = turniej[i];
		}

		return krzyzuj(zwyciezcy[0], zwyciezcy[1]);

	}

	// NUMBERS OF PARAMETERS, CYCLOMATRIC COMPLEXITY
	public static void algorytmEwolucyjny(int liczbaKolorow, int iloscIteracji,
			int mutacja) throws IOException {

		String nazwa = nazwaGrafu;

		int[][] graf = wczytajGraf(nazwaGrafu);

		PokolorowanyOsobnik[] populacjaObecna = new PokolorowanyOsobnik[licznoscPopulacji];
		PokolorowanyOsobnik[] populacjaNowa = new PokolorowanyOsobnik[licznoscPopulacji];

		PokolorowanyOsobnik[][] najlepszeOsobniki = new PokolorowanyOsobnik[iloscIteracji][iloscKrzyzowan];
		PokolorowanyOsobnik[][] najgorszeOsobniki = new PokolorowanyOsobnik[iloscIteracji][iloscKrzyzowan];
		int[][] sOsobnikow = new int[iloscIteracji][iloscKrzyzowan];

		int[] najlepszeOsobnikiDruk = new int[iloscKrzyzowan];
		int[] najgorszeOsobnikiDruk = new int[iloscKrzyzowan];
		int[] sredniaOsobnikowDruk = new int[iloscKrzyzowan];

		Osobnik osobnik = new Osobnik(graf);

		for (int it = 0; it < iloscIteracji; it++) {

			for (int i = 0; i < licznoscPopulacji; i++)
				populacjaObecna[i] = new PokolorowanyOsobnik(osobnik,
						liczbaKolorow);

			for (int i = 0; i < iloscKrzyzowan; i++) {

				najlepszeOsobniki[it][i] = populacjaObecna[0];
				najgorszeOsobniki[it][i] = populacjaObecna[0];
				sOsobnikow[it][i] = populacjaObecna[0].getLiczbaKonfliktow();
				int r = generator.nextInt(4);
				int srednia = 0;
				for (PokolorowanyOsobnik po : populacjaObecna) {
					if (najlepszeOsobniki[it][i].getLiczbaKonfliktow() > po
							.getLiczbaKonfliktow())
						najlepszeOsobniki[it][i] = po;
					if (najgorszeOsobniki[it][i].getLiczbaKonfliktow() < po
							.getLiczbaKonfliktow())
						najgorszeOsobniki[it][i] = po;
					srednia += po.getLiczbaKonfliktow();
				}
				sOsobnikow[it][i] = (int) (1.35 * srednia) / licznoscPopulacji
						+ r;

				for (int z = 0; z < licznoscPopulacji; z++) {
					if (generator.nextInt(100) > 0)
						populacjaNowa[z] = turniej(populacjaObecna, 5);
					else
						populacjaNowa[z] = populacjaObecna[z];
					if (generator.nextInt(100) > mutacja)
						populacjaNowa[z] = mutuj(populacjaNowa[z]);
				}

				populacjaObecna = populacjaNowa;

			}

			max = new PrintWriter(new FileWriter("C:\\" + nazwa + "MAX.txt",
					true));
			min = new PrintWriter(new FileWriter("C:\\" + nazwa + "MIN.txt",
					true));
			avg = new PrintWriter(new FileWriter("C:\\" + nazwa + "AVG.txt",
					true));
			System.out.println("\n\n\n-------ITERACJA " + it + "------------");

			for (int i = 0; i < iloscKrzyzowan; i++) {
				System.out.println(najlepszeOsobniki[it][i]
						.getLiczbaKonfliktow()
						+ " "
						+ sOsobnikow[it][i]
						+ " "
						+ najgorszeOsobniki[it][i].getLiczbaKonfliktow());

			}

		}

		for (int i = 0; i < iloscIteracji; i++)
			for (int j = 0; j < iloscKrzyzowan; j++) {
				najlepszeOsobnikiDruk[j] += najlepszeOsobniki[i][j]
						.getLiczbaKonfliktow();
				najgorszeOsobnikiDruk[j] += najgorszeOsobniki[i][j]
						.getLiczbaKonfliktow();
				sredniaOsobnikowDruk[j] += sOsobnikow[i][j];
			}

		for (int i = 0; i < iloscKrzyzowan; i++) {
			max.println(najlepszeOsobnikiDruk[i] / iloscIteracji);
			avg.println(sredniaOsobnikowDruk[i] / iloscIteracji);
			min.println(najgorszeOsobnikiDruk[i] / iloscIteracji);
		}

	}

	// CYCLOMATRIC COMPLEXITY
	public static void algorytmZachlanny(String nazwaGrafu, int liczbaKolorow)
			throws IOException {

		int[][] graf = { { 1, 1, 1, 1, 2, 2, 3, 3, 4, 4, 4, 5, 5, 6 },
				{ 2, 4, 5, 6, 1, 3, 2, 4, 1, 3, 5, 1, 4, 1 } };

		int liczbaWierzcholkow = 6;
		int liczbaKrawedzi = 14;

		int[] tablicaKolorow = new int[liczbaWierzcholkow];
		boolean[] tablicaDostepnosciKolorow = new boolean[liczbaKolorow];

		for (int i = 0; i < tablicaKolorow.length; i++)
			tablicaKolorow[i] = -1;

		tablicaKolorow[0] = 0; // k02

		for (int i = 1; i < liczbaWierzcholkow; i++) { // k03

			for (int j = 0; j < liczbaKolorow; j++)
				tablicaDostepnosciKolorow[j] = false; // k04
			// k05
			for (int j = 0; j < liczbaKrawedzi; j++)
				if (graf[0][j] == i) {

					if (tablicaKolorow[graf[1][j] - 1] > -1) { // tutaj problem
						tablicaDostepnosciKolorow[tablicaKolorow[graf[1][j] - 1]] = true;
					}
				}
			int j = 0; // k06

			while (j + 1 < liczbaKolorow && tablicaDostepnosciKolorow[j]) { // k07
				j++;

				tablicaKolorow[i] = j;

			}

		}

		int liczbaKonfliktow = 0;

		for (int i = 0; i < graf[0].length; i++)
			if (tablicaKolorow[graf[0][i] - 1] == tablicaKolorow[graf[1][i] - 1]) {
				// System.out.println(graf[0][i] + " " +graf[1][i] );
				liczbaKonfliktow++;
			}

		System.out.println(liczbaKonfliktow / 2);

	}

}
