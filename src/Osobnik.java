import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Random;


public class Osobnik {
	private int liczbaWierzcholkow=0;
	private int liczbaKrawedzi;
	private int[][] tablicaKrawedzi;
	Random generator = new Random();
	
	public Osobnik(int liczbaWierzcholkow){
		this.liczbaWierzcholkow=liczbaWierzcholkow;
		this.liczbaKrawedzi=generator.nextInt(2*liczbaWierzcholkow)+2*liczbaWierzcholkow;
		tablicaKrawedzi= new int[2][liczbaKrawedzi];
		for(int i=0; i<liczbaWierzcholkow;i++){
			tablicaKrawedzi[0][i]=i+1;
			tablicaKrawedzi[1][i]=generator.nextInt(liczbaWierzcholkow)+1;
		}
		for(int i = liczbaWierzcholkow; i< liczbaKrawedzi;i++){				
			tablicaKrawedzi[0][i]=generator.nextInt(liczbaWierzcholkow)+1;
			tablicaKrawedzi[1][i]=generator.nextInt(liczbaWierzcholkow)+1;
		}
	//	Arrays.sort(tablicaKrawedzi);					
			
	}
	public Osobnik(int[][] tablicaKrawedzi){
		this.tablicaKrawedzi=tablicaKrawedzi;
	
		for(int i = 0; i< tablicaKrawedzi[0].length;i++)			
			if(tablicaKrawedzi[1][i] > liczbaWierzcholkow)
				this.liczbaWierzcholkow= tablicaKrawedzi[1][i];
	
		
		
}
	
	
	public void wyswietlKrawedzie() {
		for (int i = 0; i < tablicaKrawedzi[0].length; i++)
					System.out.println(tablicaKrawedzi[0][i] + " " +tablicaKrawedzi[1][i]);
	}

	public int getLiczbaWierzcholkow() {
		return liczbaWierzcholkow;
	}

	public void setLiczbaWierzcholkow(int liczbaWierzcholkow) {
		this.liczbaWierzcholkow = liczbaWierzcholkow;
	}

	public int[][] getTablicaKrawedzi() {
		return tablicaKrawedzi;
	}

	public void setTablicaKrawedzi(int[][] tablicaKrawedzi) {
		this.tablicaKrawedzi = tablicaKrawedzi;
	}
	


	
	
	
}